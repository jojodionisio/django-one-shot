from django.contrib import admin
from todos.models import TodoList, TodoItem


@admin.register(TodoList)
class TodoList(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )


# Register your models here.
@admin.register(TodoItem)
class TodoItems(admin.ModelAdmin):
    list_display = (
        "task",
        "due_date",
        "is_completed",
    )
