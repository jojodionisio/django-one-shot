from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem

# Create your views here.


def todo_list(request):
    all_todos = TodoList.objects.all()
    context = {
        "todo_list": all_todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todo_detail,
    }
    return render(request, "todos/detail.html", context)
